# profile

My profile settings.

To checkout, clone this repository to `~/.profile.d`, then get the
submoudles: `cd ~/.profile.d;git submodule init;git submodule update`.

To enable, add the following code to your `~/.profile`

```shell
#
# Source profile extensions for certain programs.
#
if test -d $HOME/.profile.d ; then
    for s in $HOME/.profile.d/*.sh ; do
        test -r $s -a ! -k $s && . $s
    done
    unset s
fi
```
