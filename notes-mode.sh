#! /bin/bash

# Time-stamp: <2022-07-10 19:40:36 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for emacs `notes-mode`.

# Author: Berthold Höllmann <berhoel@gmail.com>

NOTES_BIN_DIR=$(find "${HOME}"/.emacs.d/elpa-*/ -type d -name notes-mode-\* | sort | tail -1)
export NOTES_BIN_DIR

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa notes-mode.sh"
# End:
