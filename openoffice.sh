#! /bin/bash

# Time-stamp: <2022-07-10 19:40:36 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for OpenOffice

# Author: Berthold Höllmann <berhoel@gmail.com>

# > I use fvwm mainly with GTK2 applications.  All window
# > menus, etc. get the appropriate decorations, except for
# > OpenOffice, which comes up with a very ugly interface.
# > But it does get the decorations under Gnome and Xfce.  So
# > what is it that forces OpenOffice to look like a Gtk
# > application under them?  Is there something I should run
# > under Fvwm to get the desired effect?

# You can try setting the environment variable (bash syntax):
OOO_FORCE_DESKTOP=gnome
export OOO_FORCE_DESKTOP

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa openoffice.sh"
# End:
