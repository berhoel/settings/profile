#!/usr/bin/python3

# from https://gist.github.com/buxtonpaul/2074bc4c56f53cf37a27e3d59ca7a29b

import subprocess

with subprocess.Popen(
    "ipconfig.exe", stdout=subprocess.PIPE, errors="ignore", universal_newlines=True
) as proc:
    text = proc.stdout.read()
    for line in text.split("\n"):
        if line and line[0] != " ":
            current_section = line
            active = False
        if "Verbindungslokale IPv6-Adresse" in line:
            active = True
        if "IPv4-Adresse" in line and active and not "vEthernet" in current_section:
            vals = line.split(":")
            print(f"{vals[-1].strip()}:0")
            break
