#! /bin/bash

# Time-stamp: <2022-08-22 17:44:45 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Environment settings for GPG.

# Author: Berthold Höllmann <berhoel@gmail.com>

# set prefered gpg signing key
GPG_TTY=$(tty)
export GPG_TTY

GPGKEY=$(LANG=C gpg --list-secret-keys --keyid-format LONG |			\
         grep sec | grep -v revoked |						\
         sed "s/  */ /g" | cut -d\             -f 2 | cut -d/ -f2 | head -1)
export GPGKEY

eval $(keychain --agents gpg 2> /dev/null)

# PIDFOUND=$(pgrep gpg-agent)
# if [ -n "$PIDFOUND" ]; then
# 	export GPG_AGENT_INFO="$HOME/.gnupg/S.gpg-agent:$PIDFOUND:1"
# 	export GPG_TTY=$(tty)
# 	export SSH_AUTH_SOCK="$HOME/.gnupg/S.gpg-agent.ssh"
# 	unset SSH_AGENT_PID
# fi
# PIDFOUND=$(pgrep dirmngr)
# if [ -n "$PIDFOUND" ]; then
# 	export DIRMNGR_INFO="$HOME/.gnupg/S.dirmngr:$PIDFOUND:1"
# fi
# unset PIDFOUND

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa gpg.sh"
# End:
