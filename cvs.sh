#! /bin/bash

# Time-stamp: <2022-07-10 19:40:34 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : CVS settings

# Author: Berthold Höllmann <berhoel@gmail.com>

export CVSROOT=$HOME/cvsroot

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa csv.sh"
# End:
