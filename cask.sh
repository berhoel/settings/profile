#! /bin/bash

# Time-stamp: <2022-07-10 19:40:37 hoel>

# Copyright © 2021 by Berthold Höllmann

# Task  : Shell settings for cask.

# Author: Berthold Höllmann <berhoel@gmail.com>

if [ -d "$HOME/.cask/" ] ; then
    PATH=$(utok "$HOME/.cask/bin" "$PATH")
    export PATH
fi

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa cask.sh"
# End:
