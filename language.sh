#! /bin/bash

# Time-stamp: <2022-07-10 19:40:35 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Language settings for shell.

# Author: Berthold Höllmann <berhoel@gmail.com>

# Most applications support several languages for their output.
# To make use of this feature, simply uncomment one of the lines below or
# add your own one (see /usr/share/locale/locale.alias for more codes)
# This overwrites the system default set in /etc/sysconfig/language
# in the variable RC_LANG.
#
LANG=de_DE.UTF-8 # uncomment this line for German output
# LANG=fr_FR.UTF-8              # uncomment this line for French output
# LANG=es_ES.UTF-8              # uncomment this line for Spanish output
export LANG

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa language.sh"
# End:
