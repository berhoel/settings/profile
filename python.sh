#! /bin/bash

# Time-stamp: <2022-07-11 09:28:11 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Python settings.

# Author: Berthold Höllmann <berhoel@gmail.com>

# Python packages often install scripts (executables) as well as
# Python modules. To get full use of --user installed packages, you
# may also want to put the matching executable path onto your system.
# I do this with the following lines in my ~/.bashrc file:
PY_USER_BIN=$(python -c 'import site; print(site.USER_BASE + "/bin")')
PATH=$(utok "$PY_USER_BIN" "$PATH")

PYTHONSTARTUP=$(dirname "${BASH_SOURCE[0]}")/pystartup
export PY_USER_BIN PYTHONSTARTUP

# Load pyenv automatically by adding
# the following to ~/.bashrc:
if [ -d "$HOME/.pyenv" ] ; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init --path)"
fi

# pip comes with support for command line completion in bash.
eval "$(pip completion --bash)"

# Enable poetry.
if [ -x "$(command -v poetry)" ] || [ -f "${HOME}"/.poetry/env ]; then
    # shellcheck source=/dev/null
    [ -f "${HOME}"/.poetry/env ] && source "${HOME}"/.poetry/env

    # Enable tab completion for Bash.
    eval "$(poetry completions bash)"
fi

# cleanup path
PATH=$(utok "$PATH")
export PATH

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa python.sh"
# End:
