#! /bin/bash

# Time-stamp: <2022-07-10 19:40:35 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for go programming.

# Author: Berthold Höllmann <berhoel@gmail.com>

GOPATH="$HOME/go"
PATH=$(utok "$PATH" "$GOPATH/bin")
export GOPATH PATH

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa go.sh"
# End:
