#! /bin/bash

# Time-stamp: <2024-05-09 18:10:59 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : EDITOR settings for bash.

# Author: Berthold Höllmann <berhoel@gmail.com>

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit

export EDITOR="vi -e"

export VISUAL="emacsclient --alternate-editor=vim"

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa editor.sh"
# End:
