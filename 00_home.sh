#! /bin/bash

# Time-stamp: <2023-01-29 19:13:42 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for applications in the home directory.

# Author: Berthold Höllmann <berhoel@gmail.com>

# set -vx

PY_UTOK=$(dirname "${BASH_SOURCE[0]}")/pyutok/utok.py

utok() {
    python "${PY_UTOK}" "$@"
}

# set +vx

PATH=$(utok "${PATH}" "${HOME}/bin")
export PATH

# initialize ssh-agent
[ -z "$SSH_AGENT_PID" ] && eval "$(ssh-agent -s)"

if [[ $- == *i* ]]
then # shell is interactive
    eval "$(dircolors --bourne-shell)"
    LS_OPTIONS="-N --color=always -T 0"
    export LS_OPTIONS
fi

# Local Variables:
# compile-command: "shellcheck -xa 00_home.sh"
# End:
