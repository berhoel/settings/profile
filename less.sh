#! /bin/bash

# Time-stamp: <2024-05-05 21:41:32 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for less.

# Author: Berthold Höllmann <berhoel@gmail.com>

# SuSE Linux: Versionen ab 8.0
#
# Symptom
#  less bricht lange Zeilen um.
# Ursache
#  Die entsprechende Parameter wurde ab der SuSE Linux Version 8.0
#  geändert, da nun nicht mehr geraten werden muß, ob sich außerhalb
#  des Fenster noch Informationen befinden.
# Lösung
#  Wenn Sie die Ausgabe von less unübersichtlich finden und die
#  Einstellung insofern ändern möchten, daß less lange Zeilen
#  "abschneidet" (wie in früheren SuSE Linux Versionen), tragen Sie
#  bitte in der Datei .profile in Ihrem Home-Verzeichnis den folgenden
#  Parameter ein:
LESS="-M -I -S -FR -r"
export LESS

# The aim of lesspipe.sh is to enhance the output of less. The choice of the rules to
# be applied to modify the output are based on the file contents.  The file extension
# is respected only as a last resort.  Usually lesspipe.sh is called as an input filter
# to less.
if command -v lesspipe.sh &> /dev/null
then
    eval $(lesspipe.sh)
fi

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa less.sh"
# End:
