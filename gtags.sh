#! /bin/bash

# Time-stamp: <2022-07-10 19:40:32 hoel>

# Copyright © 2021 by Berthold Höllmann

# Task  : Settings for gtags.

# Author: Berthold Höllmann <berhoel@gmail.com>

GTAGSLABEL=pygments
export GTAGSLABEL

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa gtags.sh"
# End:
