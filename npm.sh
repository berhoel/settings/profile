#! /bin/bash

# Time-stamp: <2024-05-05 22:06:19 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : User directory NPM settings.

# Author: Berthold Höllmann <berhoel@gmail.com>

# installing nvm
# curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
# nvm install stable

# intitlalize nvm
NVM_DIR="${HOME}/.nvm"
# shellcheck source=/dev/null
[ -s "${NVM_DIR}/nvm.sh" ] && . "${NVM_DIR}/nvm.sh"  # This loads nvm
# shellcheck source=/dev/null
[ -s "${NVM_DIR}/bash_completion" ] && . "${NVM_DIR}/bash_completion" # This loads nvm bash_completion
export NVM_DIR

# installed node packages:
# > npm -g ls -depth=0
# /home/hoel/.nvm/versions/node/v21.6.2/lib
# ├── @fsouza/prettierd@0.25.3
# ├── @socketsecurity/cli@0.9.3
# ├── @taplo/cli@0.7.0
# ├── @unibeautify/cli@0.5.1
# ├── bash-language-server@5.2.0
# ├── corepack@0.28.0
# ├── dockerfile-language-server-nodejs@0.11.0
# ├── http-server@14.1.1
# ├── install@0.13.0
# ├── magit-stats@1.0.20-dev
# ├── mapbox@1.0.0-beta10
# ├── n@9.2.3
# ├── node-red@3.1.9
# ├── npm-check-updates@16.14.20
# ├── npm@10.7.0
# ├── prettier-plugin-toml@2.0.1
# ├── prettier@3.2.5
# ├── prompts@2.4.2
# ├── pyright@1.1.361
# ├── svgo@3.2.0
# ├── tsc@2.0.4
# ├── typescript-language-server@4.3.3
# ├── vscode-css-languageserver-bin@1.4.0
# └── yarn@1.22.22
#
# npm install -g prettier prettier-plugin-toml --save-dev --save-exact
# npm install -g @fsouza/prettierd @socketsecurity/cli @taplo/cli @unibeautify/cli bash-language-server corepack dockerfile-language-server-nodejs http-server install magit-stats mapbox n node-red npm-check-updates npm prettier-plugin-toml prettier prompts pyright svgo tsc typescript-language-server vscode-css-languageserver-bin yarn

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa npm.sh"
# End:
