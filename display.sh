#! /bin/bash

# Time-stamp: <2022-07-10 19:40:32 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for X.

# Author: Berthold Höllmann <berhoel@gmail.com>

if [ -n "$WSL_DISTRO_NAME" ] ; then

    GDK_SCALE=0.5
    GDK_DPI_SCALE=1
    LIBGL_ALWAYS_INDIRECT=1
    # export DISPLAY=:0 # in WSL 1
    DISPLAY=$(python "$(dirname "${BASH_SOURCE[0]}")/wsldisplay.py")
    FREETYPE_PROPERTIES="truetype:interpreter-version=35 cff:no-stem-darkening=1 autofitter:warping=1"
    export GDK_SCALE GDK_DPI_SCALE LIBGL_ALWAYS_INDIRECT DISPLAY FREETYPE_PROPERTIES

    # OPTIONAL Set the keyboard layout to DE
    setxkbmap -layout de

    # eval $(dbus-launch --exit-with-x11)

    eval "$(dbus-launch)"
    export DBUS_SESSION_BUS_ADDRESS DBUS_SESSION_BUS_PID

fi

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa display.sh"
# End:
