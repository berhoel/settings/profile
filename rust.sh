#! /bin/bash

# Time-stamp: <2022-07-10 19:40:31 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Settings for rust/cargo compilations.

# Author: Berthold Höllmann <berhoel@gmail.com>

# Tell our environment about user-installed cargo tools
PATH=$(utok "$HOME/.cargo/bin" "$PATH")
export PATH

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa rust.sh"
# End:
