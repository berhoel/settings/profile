#! /bin/bash

# Time-stamp: <2022-07-10 19:40:35 hoel>

# Copyright © 2020 by Berthold Höllmann

# Task  : Set ORGANIZATION.

# Author: Berthold Höllmann <berhoel@gmail.com>

case $HOST in
pchoel)
	ORGANIZATION="Berthold Höllmann"
	;;
*)
	ORGANIZATION="kumkeo GmbH"
	;;
esac

export ORGANIZATION

# Local Variables:
# mode: shell-script
# coding: utf-8
# compile-command: "shellcheck -xa organization.sh"
# End:
